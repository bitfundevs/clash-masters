var express = require("express");
const request = require('request');
var cors = require('cors');
const bodyParser = require('body-parser')

var app = express();

app.use(cors());
app.use(bodyParser.json())

//let bearerTok = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjQzYjNlZjZjLTBlOGQtNDQ0OC04Nzg1LTJkNWI3NTE3NGY0MSIsImlhdCI6MTU2MjExMjMwNCwic3ViIjoiZGV2ZWxvcGVyL2E1ZWQ1OTVjLWVjZWItYTlmYi1mNDVmLTYwYjY2ZTdiMjBiNSIsInNjb3BlcyI6WyJyb3lhbGUiXSwibGltaXRzIjpbeyJ0aWVyIjoiZGV2ZWxvcGVyL3NpbHZlciIsInR5cGUiOiJ0aHJvdHRsaW5nIn0seyJjaWRycyI6WyIyMDkuMTIyLjEzLjIyNiJdLCJ0eXBlIjoiY2xpZW50In1dfQ.W2R9VhnKOjFrKsEF0c7MhAYcLqv_xiBvIIOkmWKX5fZQSlwrR07je84N1S2-BPN_k-aZ--DMizlJY-xRX3mHbA';
let bearerTok = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjU1Y2RkYmJjLTUzYWMtNGQ2My1iNjZjLTIwY2FiMmFjZDk5MSIsImlhdCI6MTU2MzAzNjg4Niwic3ViIjoiZGV2ZWxvcGVyL2E1ZWQ1OTVjLWVjZWItYTlmYi1mNDVmLTYwYjY2ZTdiMjBiNSIsInNjb3BlcyI6WyJyb3lhbGUiXSwibGltaXRzIjpbeyJ0aWVyIjoiZGV2ZWxvcGVyL3NpbHZlciIsInR5cGUiOiJ0aHJvdHRsaW5nIn0seyJjaWRycyI6WyI2OC4xODMuNDguMTgyIl0sInR5cGUiOiJjbGllbnQifV19.GBVK8w0DW00G33AXD8gK5eGZuw92Uc55Q12tv4Wnk4ULxf7G2nNY6Bbrk9OIG_c7MZdKsBoQKNItH2LWYVvfGw';

//cant be const because needs to be modifiable for each route
//example: options.url = something
var options = {
    url: 'https://api.clashroyale.com/v1',
    headers: {
      'User-Agent': 'request',
    },
    auth : {
        'bearer': bearerTok
    }
};

function callback(error, response, body) {

  //log all possible occurences including errors
  console.log(error);
  console.log(response);
  console.log(body);

  //parse the callback body
  try {
  //let parsed = JSON.parse(body);
  } catch(e) {
    console.log(e);
  }
  /*

    TODO: json can now be used in javascript form and start using the data

  */
}

app.get('/players/:tag', (req, res) => {  
  //append the provided params (also the string replacement is to foribly change the hash char to its url encoded version of itself)
  options.url += ('/players/' + req.params.tag).replace('#', '%23');
  res.send(request(options, callback));
});

//players endpoint
app.get('/players/:tag/:actions', (req, res) => {  
  //append the provided params (also the string replacement is to foribly change the hash char to its url encoded version of itself)
  
  if(req.params.actions !== null && req.params.actions !== "") {
    options.url += ('/players/' + req.params.tag + '/' + req.params.actions).replace('#', '%23');
  } else {
    options.url += ('/players/' + req.params.tag).replace('#', '%23');
  }
  res.send(request(options, callback));
});

//cards endpoint
app.get('/cards',  (req, res) => { 
  options.url += '/cards';
  res.send(request(options, callback));
});

//start the express server on port :3000
app.listen(3000, () => {
	console.log("listenting on port 3000")
});
