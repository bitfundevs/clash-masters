import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SitepurposeComponent } from './sitepurpose.component';

describe('SitepurposeComponent', () => {
  let component: SitepurposeComponent;
  let fixture: ComponentFixture<SitepurposeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SitepurposeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SitepurposeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
