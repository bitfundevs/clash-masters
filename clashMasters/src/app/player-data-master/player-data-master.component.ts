import { Component, OnInit } from '@angular/core';
import { HttputilsService } from '../services/_httputils/httputils.service';

@Component({
  selector: 'app-player-data-master',
  templateUrl: './player-data-master.component.html',
  styleUrls: ['./player-data-master.component.css']
})
export class PlayerDataMasterComponent implements OnInit {

  constructor(private http: HttputilsService) {}

  ngOnInit() {
  }

}
