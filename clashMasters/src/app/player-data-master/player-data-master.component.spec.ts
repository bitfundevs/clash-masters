import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerDataMasterComponent } from './player-data-master.component';

describe('PlayerDataMasterComponent', () => {
  let component: PlayerDataMasterComponent;
  let fixture: ComponentFixture<PlayerDataMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerDataMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerDataMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
