import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationTopPlayerComponent } from './location-top-player.component';

describe('LocationTopPlayerComponent', () => {
  let component: LocationTopPlayerComponent;
  let fixture: ComponentFixture<LocationTopPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationTopPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationTopPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
