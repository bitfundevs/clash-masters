import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeboardsComponent } from './homeboards.component';

describe('HomeboardsComponent', () => {
  let component: HomeboardsComponent;
  let fixture: ComponentFixture<HomeboardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeboardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeboardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
