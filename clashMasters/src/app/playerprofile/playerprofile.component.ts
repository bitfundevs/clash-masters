import { Component, OnInit } from '@angular/core';
import { HttputilsService } from '../services/_httputils/httputils.service';

@Component({
  selector: 'app-playerprofile',
  templateUrl: './playerprofile.component.html',
  styleUrls: ['./playerprofile.component.css']
})
export class PlayerprofileComponent implements OnInit {

  constructor(private service: HttputilsService) { }

  ngOnInit() {
  }

}
