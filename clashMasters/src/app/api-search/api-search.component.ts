import { Component, OnInit } from '@angular/core';
import { HttputilsService } from '../services/_httputils/httputils.service';

@Component({
  selector: 'app-api-search',
  template: `
  <input #newUserId (keyup.enter)="newUserEnter(newUserId.value)" placeholder="#XXXXX">
  <button (click)="newUserEnter(newUserId.value)" [routerLink]="['player']">Click me</button>`,
  styleUrls: ['./api-search.component.css']
})
export class ApiSearchComponent implements OnInit {

  constructor(public serviceUserData: HttputilsService) {
  }

  newUserEnter(newAddedUId: string) {
    console.log('testing');
    this.serviceUserData.sendGet('/players/' + newAddedUId).subscribe((data) => {
      console.log(data);
    });
  }

  ngOnInit() {
  }

}
