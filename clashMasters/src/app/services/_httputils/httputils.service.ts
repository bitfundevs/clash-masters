import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

interface PlayerData {
  name: string;
  id: string;
}

@Injectable({
  providedIn: 'root'
})
export class HttputilsService {

  private playerDataObs: Observable<PlayerData[]>;
  private CONST_API = 'http://68.183.48.182';

  constructor(private http: HttpClient) {}

   sendGet(endpoint: string) {
      return this.http.get<PlayerData[]>(this.CONST_API + endpoint);
   }

  sendPost(endpoint: string) {

  }
}
