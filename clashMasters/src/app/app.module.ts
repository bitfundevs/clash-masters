import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import 'rxjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlayerprofileComponent } from './playerprofile/playerprofile.component';
import { HeaderComponent } from './header/header.component';
import { ApiSearchComponent } from './api-search/api-search.component';
import { LocationTopPlayerComponent } from './location-top-player/location-top-player.component';
import { TournamentsComponent } from './tournaments/tournaments.component';
import { SitepurposeComponent } from './sitepurpose/sitepurpose.component';
import { FooterComponent } from './footer/footer.component';
import { HomeboardsComponent } from './homeboards/homeboards.component';
import { PlayerDataMasterComponent } from './player-data-master/player-data-master.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    PlayerprofileComponent,
    HeaderComponent,
    ApiSearchComponent,
    LocationTopPlayerComponent,
    TournamentsComponent,
    SitepurposeComponent,
    FooterComponent,
    HomeboardsComponent,
    PlayerDataMasterComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
